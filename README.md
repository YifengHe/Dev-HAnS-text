# HAnS - Helping Annotate Software #
*`Version 1.0`*

HAnS is a plugin that aims to help developers with annotating software. The annotations themselves are a proven help with
locating features, so called feature location.

[Demo video](https://youtu.be/cx_-ZshHLgA)

HAnS-text supports:
* Feature Annotation Languages
* Syntax Highlighting
* Code Completion
* Feature Model View
* Referencing
* Refactoring
* Live templates
* New files

### How to install? ###

* HAnS text can easily be installed as a plugin for IntelliJ. To set it up you need IntelliJ
  2021.1 or later installed and the HAnS 1.0 plugin downloaded.

### Contribution guidelines ###

* At the moment we are not accepting any contributions. This will be set up shortly.

### Who do I talk to? ###

* @Johan Martinson or @Herman Jansson
* Other community or team contact: Mukelabai Mukelabai 